const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false; 
  }
  let result1 = /\D/.test(str1);
  /\D/.lastIndex = 0;
  let result2 = /\D/.test(str2);
  let sum;
  if (result1 || result2) {
    return false;
  } else {
    sum = Number(str1)+Number(str2);
    sum = String(sum);
  }
  console.log(result1, result2);
  return sum;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count_posts = 0; let count_comments = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      count_posts++;
    }
    for (let key in listOfPosts[i]) {
      if (key === 'comments') {
        for (let j = 0; j < listOfPosts[i][key].length; j++) {
          if (listOfPosts[i][key][j].author === authorName) {
            count_comments++;
          }
        }
      }
    }
  }
  return `Post:${count_posts},comments:${count_comments}`;
};

const tickets = (people) => {
  let count25 = 0;
  let count50 = 0;
  for (let i = 0; i < people.length; i++) {
    switch (people[i]) {
      case 25:
        count25++;
        break;
      case 50:
        if (count25 > 0) {
          count50++;
          count25--;
        } else {
          return 'NO';
        }
        break;
      default:
        if (count50 > 0 && count25 > 0) {
          count50--;
          count25--;
        } else {
          if (count50 === 0 && count25 > 2) {
            count25-=3;
          } else {
            return 'NO';
          }
        }
    }
  }
  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
